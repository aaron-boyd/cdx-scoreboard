from sqlalchemy import create_engine
from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import pickle


def all_friendlynames(db_type):
    with DBInterface() as (_, ctx_session):
        data = ctx_session.query(db_type).all()
        return [d.friendlyname for d in data if d is not None]


def all_ids(db_type):
    with DBInterface() as (_, ctx_session):
        data = ctx_session.query(db_type).all()
        return [str(d.id) for d in data if d is not None]


def ensure_session(func):
    def inner(self, *args, **kwargs):
        if self.session is None:
            raise Exception("Database session is not established.")
        return func(self, *args, **kwargs)
    return inner


class DBConnectionManager():
    filename = "databases.pickle"

    def _read(self):
        with open(self.filename, 'rb') as fp:
            data = pickle.load(fp)
        return data

    def get_current_conn(self):
        try:
            data = self._read()
        except FileNotFoundError:
            raise FileNotFoundError("No databases have been added yet")
        return data.conn_list[data.active_conn]

    def _save(self, data):
        assert isinstance(data, DBInfoFile)
        with open(self.filename, 'wb') as fp:
            pickle.dump(data, fp)


class DBInfo(object):
    # Simple class to represent and save database connections in the pickle file
    conn_id = None
    conn_string = None

    def __repr__(self):
        return f"{self.conn_id}: {self.conn_string}"


class DBInfoFile(object):
    # simple list with a moveable index value so we can add and select DBInfo objects
    conn_list = []
    active_conn = 0

    def __sizeof__(self):
        return len(self.conn_list)


class DBInterface():
    # WEBAPP TODO: the engine used by this object should be a global singleton per sqlalchemy docs, however, switching DB's may cause issues so it's fine for now
    def __init__(self, conn_str=None):
        conn_str = conn_str if conn_str else DBConnectionManager().get_current_conn().conn_string
        self.engine = create_engine(conn_str)
        self.session_maker = sessionmaker(bind=self.engine)

    def __enter__(self):
        self.connect()
        self.session = self.session_maker()
        return (self.engine, self.session)

    def __exit__(self, type, value, traceback):
        # cleanup here
        self.commit()

    def connect(self):
        #Base.metadata.create_all(bind=self.engine) This line creates the whole db structure??
        #self.session = Session()
        pass

    @ensure_session
    def commit(self):
        self.session.commit()


