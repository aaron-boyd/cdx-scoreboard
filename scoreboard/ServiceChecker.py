import os
import re
import sys
import paramiko
import ftplib
from subprocess import getstatusoutput
import mysql.connector
import requests

from scoreboard.dbtypes.Team import Team
from scoreboard.tui import print_object_table, log_error, log_success, log_warn, log_normal

class ServiceChecker:
    FTP = 21
    SSH = 22
    HTTP = 80
    MYSQL = 3306
    POSTGRES = 5432

    @classmethod
    def check(cls, team, team_names):
        ip = team.defense_address
        ftp = cls.check_ftp(ip)
        cls._service_log(ip, 'FTP', ftp)
        ssh = cls.check_ssh(ip)
        cls._service_log(ip, 'SSH', ssh)
        mysql = cls.check_mysql(ip)
        cls._service_log(ip, 'MYSQL', mysql)
        postgres = cls.check_postgresql(ip)
        cls._service_log(ip, 'POSTGRES', postgres)
        http, http_king = cls.check_http(ip, team_names)
        cls._service_log(ip, 'HTTP', http)
        team.ftp = ftp
        team.ssh = ssh
        team.http = http
        team.postgres = postgres
        team.mysql = mysql
        return (sum([ftp,ssh,mysql,postgres, http]), http_king)

    @classmethod
    def check_service(cls, ip, port):
        # log_normal(f'Checking {ip}:{port}...')
        ret_code, _ = getstatusoutput(f'nc -z -w 3 {ip} {port}')
        return ret_code

    @classmethod
    def check_ftp(cls, ip):
        points = 0
        try:
            ftp = ftplib.FTP(ip, 'anonymous', timeout=3)
            points += 1
            ftp.cwd('/tmp')
            with open('test.txt', 'rb') as fh:
                ftp.storbinary('STOR a.txt', fh)
                points += 1
            ftp.close()
            return points
        except Exception as e:
            log_error(str(e))
            return points

    @classmethod
    def check_ssh(cls, ip):
        points = 0
        if cls.check_service(ip, cls.SSH) == 0:
            points += 1
            ret_code, output = getstatusoutput(f'ssh-keyscan {ip}')
            if cls._is_ssh_ready(ip, 'guest', None, '~/.ssh/id_rsa'):
                points += 1
        return points

    @classmethod
    def check_http(cls, ip, team_names):
        try:
            data = requests.get(f'http://{ip}')
            data = data.content.decode('utf-8')
            for name in team_names:
                if re.search(re.escape(name), data):
                    return (1, name)
            return (1, None)
        except Exception as e:
            log_error(str(e))
            return (0, None)

    @classmethod
    def check_mysql(cls, ip):
        try:
            conn = mysql.connector.connect(host=ip, user='user', passwd='password', connect_timeout=3)
            return 1
        except Exception as e:
            log_error(str(e))
            return 0

    @classmethod
    def check_postgresql(cls, ip):
        return 1 if cls.check_service(ip, cls.POSTGRES) == 0 else 0

    @classmethod
    def _is_ssh_ready(cls, ip, username, password=None, key=None):
        try:
            private_key = paramiko.RSAKey.from_private_key_file(os.path.expanduser(key))
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(ip, username=username, password=password, pkey=private_key)
            ssh.close()
            return True
        except Exception as e:
            log_error(str(e))
            return False

    @classmethod
    def _service_log(cls, ip, service, points):
        if points:
            log_success('{:15} : {:10}|{:>5} |{:>3}'.format(ip,service,'UP',points))
        else:
            log_error('{:15} : {:10}|{:>5} |{:>3}'.format(ip,service,'DOWN',points))
