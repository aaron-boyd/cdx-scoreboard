from scoreboard.dbtypes import Base, PropertyBase
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

class Team(Base, PropertyBase):
    # information related to SSH authentication to a device
    __tablename__ = 'Team'

    defense_address = Column(String(15))
    attack_address = Column(String(15))
    score = Column(Integer())
    
    ftp = Column(Integer())
    postgres = Column(Integer())
    mysql = Column(Integer())
    ssh = Column(Integer())
    http = Column(Integer())
