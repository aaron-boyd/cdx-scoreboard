from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, MetaData, String

Base = declarative_base()

class PropertyBase(object):
    id = Column(Integer, primary_key=True)
    friendlyname = Column(String(100), unique=True)
    # general base class for scoreboard

class TaggableBase(object):
    # base for classes that should be able to be 'tagged'
    # TODO: just needs a single tag obj and list of tags in the DB eventually
    pass

class Tag(Base, PropertyBase):
    # Just a logical way of tagging things, 
    # use the friendly name as the tag name
    # use TaggableBase on objects that should be taggable
    __tablename__ = 'tag'
