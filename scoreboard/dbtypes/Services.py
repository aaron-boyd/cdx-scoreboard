from scoreboard.dbtypes import Base, PropertyBase
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from scoreboard.dbtypes.Team import Team

class FTPService(Base, PropertyBase):
    # information related to SSH authentication to a device
    __tablename__ = 'FTPService'

    port = Column(Integer())

    def check(self, team):

