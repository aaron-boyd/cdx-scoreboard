from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dbtypes import *
from uritools import urisplit

def ensure_session(func):
    def inner(self, *args, **kwargs):
        if self.session is None:
            raise Exception("Database session is not established.")
        return func(self, *args, **kwargs)
    return inner

def init_database():
    with DBInterface() as (ctx_engine, _):
        Base.metadata.create_all(ctx_engine.engine)
        print("DB initialized")

class Database():

    def do_init(self, conn_str, overwrite=False):
        # Determine schema name
        # TODO: move to the connection manager side of things, we could probably use a named regex somewhere in there to make things nicer
        conn_str_no_schema = conn_str
        if urisplit(conn_str).getscheme() != 'sqlite':
            schema = urisplit(conn_str).getpath().lstrip("/")
            conn_str_no_schema = conn_str[:conn_str.rfind(schema)]
            schema = 'cyber_def_final'

        # Make new Schema or overwrite
        with DBInterface(conn_str_no_schema) as (ctx_engine, _):
            if ctx_engine.dialect.name == 'sqlite':
                pass  # no schemas or internal databases, just a file
            else:
                # Check if the schema already exists
                try:
                    ctx_engine.execute(sqlalchemy.schema.CreateSchema(schema))
                    print(f"Created {schema} in database")
                except sqlalchemy.exc.ProgrammingError:
                    if overwrite:
                        print(f"Schema {schema} already exists in the DB, initializing tables")
                        ctx_engine.execute(sqlalchemy.schema.DropSchema(schema))
                        ctx_engine.execute(sqlalchemy.schema.CreateSchema(schema))
                        print(f"{schema} schema recreated")
                    else:
                        raise Exception(f"Schema {schema} already exists in the DB, and --overwrite not specified")

        # Add tables and whatnot
        with DBInterface(conn_str) as (ctx_engine, _):
            if overwrite:
                Base.metadata.drop_all(ctx_engine.engine)
                print("Old DB flushed")
            Base.metadata.create_all(ctx_engine.engine)
            print("DB initialized")
        return





class DBInterface():

    def __init__(self, conn_str):
        self.engine = create_engine(conn_str)
        self.session_maker = sessionmaker(bind=self.engine)

    def __enter__(self):
        self.connect()
        self.session = self.session_maker()
        return (self.engine, self.session)

    def __exit__(self, type, value, traceback):
        # cleanup here
        self.commit()

    def connect(self):
        #Base.metadata.create_all(bind=self.engine) This line creates the whole db structure??
        #self.session = Session()
        pass

    @ensure_session
    def commit(self):
        self.session.commit()
