import argparse
import sys
import os
import jinja2
from subprocess import getstatusoutput

from scoreboard.commands import BaseCommand
from scoreboard.dbtypes.Team import Team
from scoreboard.tui import print_object_table, log_error, log_success, log_warn, log_normal
from scoreboard.ScoreboardDatabase import DBInterface, all_friendlynames, all_ids
from scoreboard.ServiceChecker import ServiceChecker
from time import sleep

class Runnercmd(BaseCommand):
    name = 'runner'
    description = 'Run scoreboard'

    def do_start(self, argline):
        # run contest
        parser = argparse.ArgumentParser(description='Run contest')
        parser.add_argument('--interval',
                            help='Interval to poll services (seconds)',
                            default=300,
                            type=int)
        parser.add_argument('--overwrite',
                            action='store_true',
                            help='Zero everyones scores')

        args = parser.parse_args(argline)

        if args.overwrite:
            with DBInterface() as (_, ctx_session):
                teams = ctx_session.query(Team).all()
                for team in teams:
                    team.score = 0
            log_success("Successfully reset players scores.")

        log_normal(f'Running contest with polling every {args.interval} seconds.')

        while True:
            with DBInterface() as (_, ctx_session):
                teams = ctx_session.query(Team).all()

                for team in teams:
                    log_normal(f'Polling: {team.friendlyname}...')
                    not_my_names = [item.friendlyname for item in teams if item.friendlyname != team.friendlyname]
                    points, king_of_http = ServiceChecker.check(team, not_my_names)

                    if king_of_http:
                        king = [item for item in teams if item.friendlyname == king_of_http][0]
                        king.score += 1
                        log_success(f'Team {king.friendlyname} gets a point for occupying {team.friendlyname}')

                    log_normal(f'{team.friendlyname} gets {points} points.')
                    team.score += points

                print_object_table(teams)

            self._render_html()
            sleep(args.interval)

    def _render_html(self):
        template_filename = "template.html.j2"
        rendered_filename = "/var/www/html/index.html"
        file_dir = os.path.dirname(__file__)
        template_dir = os.path.join(file_dir, '../templates/')
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))

        os.system(f'cp data.log /var/www/html/')

        with DBInterface() as (_, ctx_session):
            teams = ctx_session.query(Team).all()
            teams.sort(key = lambda x : x.score, reverse=True)
            output_text = environment.get_template(template_filename).render(items=teams)
            with open(rendered_filename, 'w') as fp:
                fp.write(output_text)







