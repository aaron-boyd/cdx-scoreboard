import argparse
import sys
import os

from scoreboard.commands import BaseCommand
from scoreboard.dbtypes.Team import Team
from scoreboard.tui import print_object_table, log_error, log_success, log_warn
from scoreboard.ScoreboardDatabase import DBInterface, all_friendlynames, all_ids

class Teamcmd(BaseCommand):
    name = 'team'
    description = 'Configure teams'

    def do_list(self, argline):
        # list libvirt networks on server
        parser = argparse.ArgumentParser(description='List teams')

        args = parser.parse_args(argline)

        with DBInterface() as (_, ctx_session):
            teams = (ctx_session.query(Team).all())

            print_object_table(teams)

    def do_add(self, argline):
        # list libvirt networks on server
        parser = argparse.ArgumentParser(description='Add team')
        parser.add_argument('name',
                            help='Name of team')
        parser.add_argument('defense_address',
                            help='Address of defending machine')
        parser.add_argument('attack_address',
                            help='Address of attacking machine')

        args = parser.parse_args(argline)

        with DBInterface() as (_, ctx_session):
            team = Team(friendlyname=args.name,
                defense_address=args.defense_address,
                attack_address=args.attack_address,
                score=0)

            ctx_session.add(team)

    def do_rm(self, argline):
        # remove a team
        parser = argparse.ArgumentParser(description='Remove team from database')
        _group = parser.add_mutually_exclusive_group(required=True)
        _group.add_argument('--id',
                            help='id of Team',
                            choices=all_ids(Team))
        _group.add_argument('--name',
                            help='name of Team',
                            choices=all_friendlynames(Team))

        args = parser.parse_args(argline)

        with DBInterface() as (_, ctx_session):
            team = (ctx_session.query(Team).filter_by(id=args.id).one()
                if args.id
                else ctx_session.query(Team).filter_by(friendlyname=args.name).one())

            ctx_session.delete(team)

        log_success("Successfully deleted the team.")

