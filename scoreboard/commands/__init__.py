import argparse
from os.path import dirname, basename, isfile, join
import glob
import importlib

from abc import ABC, abstractmethod


class BaseCommand(ABC):
    # inspired by pip
    name = None
    description = None
    hidden = False

    def __init__(self):
        pass

    def subparser(self):
        parser = argparse.ArgumentParser(prog=self.name, description=self.description)
        parser.add_argument("subcommand", choices=[f[3:] for f in dir(self) if f.startswith("do_")])
        return parser

    def __call__(self):
        # returning none for subparser() lets the command object be called directly
        pass

    def run_command(self, argline: str):
        if self.subparser() is not None:
            # New test that dynamically rips the do_x choices from the local class
            # more compact implementation of the db subcommand parser that isn't a billion if statements
            args = self.subparser().parse_args(argline[:1])
            real_func = getattr(self, f"do_{args.subcommand}", None)
            return real_func(argline[1:])
        else:
            # Add an option called default
            # This allows non-tiered commands
            # Command must implement __call__
            return self(argline)


class Task(ABC):
    name = None  # Human name for task
    parser = None  # argparse parser

    def execute(self):
        return


# Import all commands automatically from commands package
# stackoverflow.com/questions/1057431 language abused
modules = glob.glob(join(dirname(__file__), "*.py"))
__all__ = [basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]
[importlib.import_module(f".{mod}", package="scoreboard.commands") for mod in __all__]

command_list = BaseCommand.__subclasses__()

command_dict = dict()
for cmd in command_list:
    command_dict[cmd.name] = cmd
