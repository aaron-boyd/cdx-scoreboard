import pickle
import argparse
import shlex
import sqlalchemy
from uritools import urisplit

from scoreboard.ScoreboardDatabase import DBInterface, DBConnectionManager, DBInfo, DBInfoFile
from scoreboard.commands import BaseCommand
from scoreboard.dbtypes import Base
from scoreboard.dbtypes.Team import Team

from scoreboard.tui import log_error, log_success, log_warn, log_normal

class DBcmd(BaseCommand):
    name = 'db'
    description = ('Configure Scoreboard Database'+
            '\n\tScoreboard uses SQLAlchemy to abstract internal data structures'
    )

    def __init__(self):
        super().__init__()

    def do_list(self, argline=""):
        parser = argparse.ArgumentParser(description="List the available DB connections")
        parser.parse_args(argline)
        try:
            data = DBConnectionManager()._read()
            log_normal(f"Using connection {data.active_conn + 1}")
            for index in range(len(data.conn_list)):
                if index == data.active_conn:
                    print(f"-->\t{data.conn_list[index]}")
                else:
                    print(f"\t{data.conn_list[index]}")
        except FileNotFoundError:
            log_warn("No databases have been added yet")

    def do_add(self, argline):
        # TODO: this should validate that the string added
        # is a valid range master DB or it should otherwise
        # fail and tell the user to run db init instead
        parser = argparse.ArgumentParser(description="Add a database connection to the known database list")
        parser.add_argument("connection_string")
        parser.add_argument("--no-check",
                            action='store_true',
                            help='Skip checking the connection')
        args = parser.parse_args(argline)
        conn_string = args.connection_string
        assert conn_string is not None and len(conn_string)>0, "Please specify a connection string"

        try:
            data = DBConnectionManager()._read()
        except:
            data = DBInfoFile()

        conn = DBInfo()
        conn.conn_string = conn_string
        conn.conn_id = len(data.conn_list) + 1

        assert conn.conn_string not in [c.conn_string for c in data.conn_list], "Database already exists in config file"
        data.conn_list += [conn]

        DBConnectionManager()._save(data)
        log_success(f"Added {conn} to the stored connections")

    def do_use(self, argline):
        parser = argparse.ArgumentParser(description="Selects a Preconfigured DB connection to use")
        parser.add_argument("index",
                            type=int)
        index = parser.parse_args(argline).index
        index = index - 1  # -1 converts from human list to python list index
        try:
            data = DBConnectionManager()._read()
        except:
            data = DBInfoFile()
        assert index >= 0 and index < len(data.conn_list), "Index must be in range of saved connections"
        data.active_conn = index
        DBConnectionManager()._save(data)
        self.do_list() # TODO: should not call do_'s directly

    def do_init(self, argline):
        parser = argparse.ArgumentParser(description="Initialize a database for use with Scoreboard")
        parser.add_argument("--overwrite",
                            default=False,
                            action='store_true',
                            help="Overwrite an existing schema if one exists")
        parser.add_argument("--schema",
                            default="scoreboard",
                            help="Schema to use in the datebase if not specified in connection string")
        args = parser.parse_args(argline)

        # Determine schema name
        # TODO: move to the connection manager side of things, we could probably use a named regex somewhere in there to make things nicer
        conn_str = DBConnectionManager().get_current_conn().conn_string
        conn_str_no_schema = conn_str
        if urisplit(conn_str).getscheme() != 'sqlite':
            schema = urisplit(conn_str).getpath().lstrip("/")
            conn_str_no_schema = conn_str[:conn_str.rfind(schema)]
            schema = args.schema if schema == '/' and args.schema else schema

        # Make new Schema or overwrite
        with DBInterface(conn_str_no_schema) as (ctx_engine, _):
            if ctx_engine.dialect.name == 'sqlite':
                pass  # no schemas or internal databases, just a file
            else:
                # Check if the schema already exists
                try:
                    ctx_engine.execute(sqlalchemy.schema.CreateSchema(schema))
                    log_success(f"Created {schema} in database")
                except sqlalchemy.exc.ProgrammingError:
                    if args.overwrite:
                        log_normal(f"Schema {schema} already exists in the DB, initializing tables")
                        ctx_engine.execute(sqlalchemy.schema.DropSchema(schema))
                        ctx_engine.execute(sqlalchemy.schema.CreateSchema(schema))
                        log_success(f"{schema} schema recreated")
                    else:
                        raise Exception(f"Schema {schema} already exists in the DB, and --overwrite not specified")

        # Add tables and whatnot
        with DBInterface() as (ctx_engine, _):
            if args.overwrite:
                Base.metadata.drop_all(ctx_engine.engine)
                log_success("Old DB flushed")
            Base.metadata.create_all(ctx_engine.engine)
            log_success("DB initialized")
        return

    def do_rm(self, argline):
        parser = argparse.ArgumentParser(usage="Remove specified DB from Scoreboard.")
        parser.add_argument("index",
                            type=int)
        index = parser.parse_args(argline).index
        index -= 1
        data = DBConnectionManager()._read()

        try:
            del data.conn_list[index]

            if data.active_conn == index:
                data.active_conn = 0
            elif data.active_conn > index:
                data.active_conn -= 1
            for i in range(len(data.conn_list)):
                data.conn_list[i].conn_id = i + 1
            log_success("Database removed.")
        except IndexError:
            log_error("An invalid index was entered.")

        DBConnectionManager()._save(data)

    def do_test(self, argline):
        """Tests current db connection"""
        current_conn = DBConnectionManager().get_current_conn()
        with DBInterface() as (ctx_engine, ctx_session):
            try:
                ctx_engine.connect()
                log_success(f"Connected to {current_conn.conn_string} successfully")
            except:
                log_error("Engine connection failed, this may indicate a permissions issue")
                raise
            try:
                ctx_session.query(SiloServer).all()
                log_success("Database is already initialized")
            except:
                log_error("Database test query failed, this may indicate the DB needs initialized, try:")
                log_error("scoreboard.py db init")
                raise
        return
